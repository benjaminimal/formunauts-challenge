from typing import Any, List, Union, Set, Dict
import math


class Validator:
    """
    A utility class for simple validation functions
    """

    @staticmethod
    def is_number(x: Any) -> bool:
        """
        Tests if ``x`` is an int or float.

        :param x:   The value to validate.
        :return:    Whether validation succeeded.
        """
        return isinstance(x, int) \
               or (isinstance(x, float)
                   and not math.isnan(x)
                   and not math.isinf(x))

    @staticmethod
    def is_blank(x: Any) -> bool:
        """
        Tests if ``x`` is a non whitespace only string.

        :param x:   The value to validate.
        :return:    Whether validation succeeded.
        """
        return isinstance(x, str) and len(x.strip()) == 0

    @staticmethod
    def is_in(x: Any, domain: Union[List, Set, Dict]) -> bool:
        """
        Test if ``x`` can be found in ``domain``

        :param x:       The value to validate.
        :param domain:  A list of values that x can take.
        :return:        Whether validation succeeded.
        """
        return domain is not None and x in domain

    @staticmethod
    def is_greater(x: Any, bound: Any, check_equality: bool = False) -> bool:
        """
        Test if ``x`` is greater than ``bound``.

        :param x:               The value to test.
        :param bound:           The bound to test against.
        :param check_equality:  If True equal values are ok as well.
        :return:                Whether validation succeeded.
        """
        return x is not None \
               and bound is not None \
               and (x >= bound if check_equality else x > bound)

    @staticmethod
    def is_less(x: Any, bound: Any, check_equality: bool = False) -> bool:
        """
        Test if ``x`` is less than ``bound``.

        :param x:               The value to test.
        :param bound:           The bound to test against.
        :param check_equality:  If True equal values are ok as well.
        :return:                Whether validation succeeded.
        """
        return x is not None \
               and bound is not None \
               and (x <= bound if check_equality else x < bound)
