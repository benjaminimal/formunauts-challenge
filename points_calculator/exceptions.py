class ValidationError(Exception):
    """Used to indicate malformed input"""
    pass


class MissingDataError(Exception):
    """Used to indicate that data is missing."""
    pass


class FormatError(Exception):
    """Used to indicate the wrong format of something e.g a file."""
    pass
