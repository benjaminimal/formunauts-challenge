from typing import Optional, Dict, List
import re

import pandas as pd
from xlrd import XLRDError

from points_calculator.exceptions import ValidationError, MissingDataError, FormatError
from points_calculator.validator import Validator


class PointsCalculator:
    """
    A class to process data from excel sheets.
    Expects that at least the following column headers or keys are present.

    amount, age, payment_method, payment_interval, expected_points
    """

    AMOUNT_KEY = 'amount'
    AGE_KEY = 'age'
    PAYMENT_METHOD_KEY = 'payment_method'
    PAYMENT_INTERVAL_KEY = 'payment_interval'
    EXPECTED_POINTS_KEY = 'expected_points'

    DIRECT_DEBIT = 'direct_debit'
    CREDIT_CARD = 'credit_card'

    REQUIRED_COLUMN_NAMES = {
        AMOUNT_KEY,
        AGE_KEY,
        PAYMENT_METHOD_KEY,
        PAYMENT_INTERVAL_KEY,
        EXPECTED_POINTS_KEY,
    }

    VALID_PAYMENT_METHODS = {
        DIRECT_DEBIT,
        CREDIT_CARD,
    }

    VALID_PAYMENT_INTERVALS = {
        0,  # one of
        1,  # annual
        2,  # biannual
        4,  # quarterly
        12,  # monthly
    }

    VALIDATORS = {
        AMOUNT_KEY: lambda x: Validator.is_number(x) and Validator.is_greater(x, 0),
        AGE_KEY: lambda x: Validator.is_number(x) and Validator.is_greater(x, 0),
        PAYMENT_METHOD_KEY: lambda x: Validator.is_in(x, PointsCalculator.VALID_PAYMENT_METHODS),
        PAYMENT_INTERVAL_KEY: lambda x: Validator.is_number(x)
                                        and Validator.is_in(x, PointsCalculator.VALID_PAYMENT_INTERVALS),
        EXPECTED_POINTS_KEY: lambda x: Validator.is_number(x) and Validator.is_greater(x, 0),
    }

    def __init__(self):
        self._src_file = None
        self._list_data = None
        self._data_frame = None
        self._valid = False

    def read_excel(self, filename: str) -> None:
        """
        Read an excel file for further processing.
        Possible file extensions are xls, xlsx, xlsm, xlsb, odf, ods and odt.

        :raises FormatError:        When a non matching filetype is processed.
        :raises ValidationError:    For missing headers, data or invalid fields.

        :param filename:    A path to a local file or a url.
        :return:            None
        """
        self._src_file = filename

        # Load the file
        try:
            self._data_frame = pd.read_excel(self._src_file)
        except XLRDError as exc:
            raise FormatError(exc)

        self._reformat_and_validate()

    def read_list(self, list_data: List[Dict]):
        """
        Read a list of dictionaries for further processing.

        :raises FormatError:        When the input is not valid json.
        :raises ValidationError:    For missing keys, data or invalid fields.

        :param list_data:   A list of dictionaries.
        :return:            None
        """
        self._list_data = list_data

        # Transform list to a data frame
        try:
            self._data_frame = pd.DataFrame(self._list_data)
        except ValueError as exc:
            raise FormatError(exc)

        self._reformat_and_validate()

    def _reformat_and_validate(self):
        """
        Turn headers into lower_snake_case and validate the data frame.

        :raises ValidationError:    For missing keys, data or invalid fields.

        :return:    None
        """

        # Reformat column names
        def reformater(x: str):
            return re.sub(r'\s+', r'_', x.strip().lower()) if (x is not None and isinstance(x, str)) else x

        self._data_frame.rename(columns=reformater, inplace=True)

        # Validate data_frame
        self._validate_data_frame()

    def _validate_data_frame(self) -> None:
        """
        Validate the data frame.

        :raises ValidationError:    For missing keys, data or invalid fields.

        :return:    None
        """
        # Validate column names
        column_names = set(self._data_frame.columns.values.tolist())
        name_intersection = list(column_names & self.REQUIRED_COLUMN_NAMES)
        if len(name_intersection) != len(self.REQUIRED_COLUMN_NAMES):
            self._valid = False
            raise ValidationError(
                "Missing columns {} in {}".format(', '.join(self.REQUIRED_COLUMN_NAMES - column_names), self._src_file)
            )

        # Validate entries
        for column_name in self.REQUIRED_COLUMN_NAMES:
            for i, cell in enumerate(self._data_frame[column_name]):
                if not self.VALIDATORS[column_name](cell):
                    self._valid = False
                    raise ValidationError("Malformed value for column {} in row {}".format(column_name, i + 2))

        self._valid = True

    def calculate_points(self, amount: Optional[int], age: Optional[int], payment_method: Optional[str],
                         payment_interval: Optional[int]) -> int:
        """
        Sum up the expected points from the source data on rows where fields match the arguments.
        A None argument will match any row for that column.

        :raises MissingDataError:   When no valid data has been loaded yet.

        :param amount:              The amount to match.
        :param age:                 The age to match.
        :param payment_method:      The payment_method to match.
        :param payment_interval:    The payment_interval to match.
        :return:                    The sum of expected points for the given criteria.
        """
        if not self._valid:
            raise MissingDataError("No valid data source has been loaded yet.")

        df = self._data_frame

        df = df[df[self.AMOUNT_KEY] == amount] if amount is not None else df
        df = df[df[self.AGE_KEY] == age] if age is not None else df
        df = df[df[self.PAYMENT_METHOD_KEY] == payment_method] if payment_method is not None else df
        df = df[df[self.PAYMENT_INTERVAL_KEY] == payment_interval] if payment_interval is not None else df

        return df[self.EXPECTED_POINTS_KEY].sum()
