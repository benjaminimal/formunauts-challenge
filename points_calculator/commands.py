import sys

import click

from points_calculator.core import PointsCalculator
from points_calculator.exceptions import ValidationError, FormatError


@click.command()
@click.argument('file', nargs=1, type=click.Path(file_okay=True, dir_okay=False, readable=True))
@click.option('--amount', type=int, help="The amount to query for")
@click.option('--age', type=int, help="The age to query for")
@click.option('--payment_method', type=click.Choice(PointsCalculator.VALID_PAYMENT_METHODS),
              help="The payment method to query for")
@click.option('--payment_interval', type=int, help="One of 0, 1, 2, 4, 12")
def process(file: str, amount: int, age: int, payment_method: str, payment_interval: int) -> None:
    """
    Process an excel file to calculate the total points gained.
    Setting parameters will restrict the rows being processed to those which exactly match that value.
    Omitting a parameter will match all rows.

    :param file:    The path or url of the exel file to process.
    """
    points_calculator = PointsCalculator()
    try:
        points_calculator.read_excel(file)
    except (FormatError, ValidationError, FileNotFoundError) as exc:
        click.echo(exc, err=True)
        sys.exit(1)
    point_sum = points_calculator.calculate_points(amount, age, payment_method, payment_interval)
    click.echo(point_sum)
    sys.exit(0)
