# Points Calculator

A simple tool to process excel files with the following headers.

    amount, age, payment_method, payment_interval, expected_points

It can sum `expected_points` while filtering rows based on criteria
given on the other columns.

This is the product of the [assignment](ASSIGNMENT.md) I was given
during the interview process at formunauts.

## Installation

Requirements: 

* Python3
* pip (for python 3)

1. Clone the repository and cd into the project directory.

    ```
    $ git clone https://gitlab.com/benjaminimal/formunauts-challenge.git
    $ cd formunauts-challenge
    ```
    
2. Install and setup python virtualenv

    ```
    $ pip install virtualenv
    $ virtualenv -p $(which python3) .venv
    $ source .venv/bin/activate
    ```
   
3. Install requirements

    ```
    $ pip install -r requirements.txt
    ```
   Note: in case you want to run the tests as well you need to execute
   `pip install -r points_calculator/resources/requirements/test.txt`
   which you can then run with `pytest -v`.

## Configuration

There is currently no further configuration needed.

## Usage

### Command Line Interface

After setting up the project you can run it from the command line like this.

```
$ python -m points_calculator FILENAME
```

You can find example data in `tests/resources/excel/`.

For further details on how to use the command line interface run.

```
$ python -m points_calculator --help
```

### In Python Code

Alternatively you can use the code in your own python code. For this you can
skip the virualenv step and, put the `points_calculator` directory in your
python path and make sure you have all the requirements in there as well.

For detailed information please read the docstrings. Here is an example on how to use it.

```python
from points_calculator import PointsCalculator, ValidationError, FormatError


def some_function(# ...args and kwargs...):
    # ...some code...
    pc = PointsCalculator()
    try:
        pc.read_excel(filename)
    except (ValidationError, FormatError) as exc:
        # ...error handling...
    points = pc.calculate_points(amount, age, payment_method, payment_interval)
    # ...some code...

```