from typing import Any
import os
import json


def get_test_resource(path: str):
    return os.path.join(os.path.dirname(__file__), 'resources', path)


def get_excel_filepath(filename: str) -> str:
    return get_test_resource(os.path.join('excel', filename))


def get_json_filepath(filename: str) -> str:
    return get_test_resource(os.path.join('json', filename))


def load_from_json(filename: str) -> Any:
    with open(get_json_filepath(filename)) as f:
        return json.load(f)
