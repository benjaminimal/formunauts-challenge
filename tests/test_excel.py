import pytest

from points_calculator import PointsCalculator, ValidationError, FormatError
from tests.utils import get_excel_filepath


def test_empty_file_no_headers():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('0-entries_no-header.xlsx'))


def test_empty_file_with_header():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('0-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(None, None, None, None)
    assert point_sum == 0


def test_missing_header():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_no-header.xlsx'))


def test_header_reformat():
    pc = PointsCalculator()
    try:
        pc.read_excel(get_excel_filepath('10-entries_with-header-spaces.xlsx'))
    except ValidationError as exc:
        pytest.fail("Unexpected ValidationError when reformatting headers headers: {}".format(exc))


def test_missing_column():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_missing-columns.xlsx'))


def test_extra_column():
    pc = PointsCalculator()
    try:
        pc.read_excel(get_excel_filepath('10-entries_with-header_extra-column.xlsx'))
    except ValidationError as exc:
        pytest.fail("Unexpected ValidationError when reading additional columns: {}".format(exc))


def test_with_formulas():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header_formulas.xlsx'))
    point_sum = pc.calculate_points(None, None, None, None)
    assert point_sum == 1050


def test_empty_cell_age():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_empty-age.xlsx'))


def test_empty_cell_amount():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_empty-amount.xlsx'))


def test_empty_cell_expected_points():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_empty-expected-points.xlsx'))


def test_empty_cell_payment_interval():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_empty-payment-interval.xlsx'))


def test_empty_cell_payment_method():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_empty-payment-method.xlsx'))


def test_wrong_type_age():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_type-error-age.xlsx'))


def test_wrong_type_amount():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_type-error-amount.xlsx'))


def test_wrong_type_expected_points():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_type-error-expected-points.xlsx'))


def test_wrong_type_payment_interval():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_type-error-payment-interval.xlsx'))


def test_wrong_type_payment_method():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_excel(get_excel_filepath('10-entries_with-header_type-error-payment-method.xlsx'))


def test_specific_amount():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(10, None, None, None)
    assert point_sum == 424


def test_specific_age():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(None, 18, None, None)
    assert point_sum == 148


def test_specific_payment_method():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(None, None, 'direct_debit', None)
    assert point_sum == 843


def test_specific_payment_interval():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(None, None, None, 4)
    assert point_sum == 364


def test_match_all():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(None, None, None, None)
    assert point_sum == 1367


def test_match_none():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(20, 28, PointsCalculator.CREDIT_CARD, 12)
    assert point_sum == 0


def test_partial_parameter_combination():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(20, None, PointsCalculator.CREDIT_CARD, None)
    assert point_sum == 154


def test_complete_parameter_combination():
    pc = PointsCalculator()
    pc.read_excel(get_excel_filepath('10-entries_with-header.xlsx'))
    point_sum = pc.calculate_points(20, 28, PointsCalculator.CREDIT_CARD, 2)
    assert point_sum == 62


def test_impostor_test_file():
    pc = PointsCalculator()
    with pytest.raises(FormatError):
        pc.read_excel(get_excel_filepath('not-an-excel-file.xlsx'))


def test_disguised_test_file():
    pc = PointsCalculator()
    try:
        pc.read_excel(get_excel_filepath('im-an-excel-file.md'))
    except FormatError as exc:
        pytest.fail("Unexpected FormatError reading disguised excel file: {}".format(exc))