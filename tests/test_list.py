import pytest
from points_calculator import PointsCalculator, ValidationError

from tests.utils import load_from_json


def test_empty():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('empty.json'))


def test_extra_key():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries_extra-key.json'))
    point_sum = pc.calculate_points(None, None, None, None)
    assert point_sum == 1367


def test_missing_age():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_missing-age.json'))


def test_missing_amount():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_missing-amount.json'))


def test_missing_expected_points():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_missing-expected-points.json'))


def test_missing_payment_interval():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_missing-payment-interval.json'))


def test_missing_payment_method():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_missing-payment-method.json'))


def test_type_error_age():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_type-error-age.json'))


def test_type_error_amount():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_type-error-amount.json'))


def test_type_error_expected_points():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_type-error-expected-points.json'))


def test_type_error_payment_interval():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_type-error-payment-interval.json'))


def test_type_error_payment_method():
    pc = PointsCalculator()
    with pytest.raises(ValidationError):
        pc.read_list(load_from_json('10-entries_type-error-payment-method.json'))


def test_sum_all():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(None, None, None, None)
    assert point_sum == 1367


def test_restrict_amount():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(50, None, None, None)
    assert point_sum == 272


def test_restrict_age():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(None, 18, None, None)
    assert point_sum == 148


def test_restrict_payment_method():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(None, None, 'direct_debit', None)
    assert point_sum == 843


def test_restrict_payment_interval():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(None, None, None, 0)
    assert point_sum == 112


def test_restrict_partial():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(None, 42, None, 0)
    assert point_sum == 107


def test_restrict_complete():
    pc = PointsCalculator()
    pc.read_list(load_from_json('10-entries.json'))
    point_sum = pc.calculate_points(90, 42, PointsCalculator.DIRECT_DEBIT, 0)
    assert point_sum == 107
