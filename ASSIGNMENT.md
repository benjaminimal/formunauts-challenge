 # Assignment: points calculator
 * Zunächst einmal soll die excel Tabelle in ein python freundliches format konvertiert werden (liste von listen, dict, oä.)
 * hierzu eine funktion schreiben die das excel file ausliest und die python struktur erstellt.
 * Das Ergebnis in einem .py file speichern, die nachfolgende Funktion soll dann die Daten von dort lesen.
 * Ergebnis soll eine Funktion `calculate_points` sein mit der man 4 Parameter übergeben kann, und die sich daraus ergebenden Punkte (laut Tabelle) returniert
   * amount: int (siehe tabelle)
   * age: int (siehe tabelle)
   * payment_method: string, either 'direct_debit' or 'credit_card'
   * payment_interval: int:
     * 12: monthly
     * 4: quarterly
     * 2: biannual
     * 1: annual
     * 0: one off
     
     Note: Beim Amount wird davon ausgegangen, dass er im jeweils richtigen Format (monthly, yearly, oneoff) ankommt, keine Umrechnung notwendig.
 * Des Weiteren eine pytest kompatible Testfunktion (https://docs.pytest.org/en/stable/). am besten werden Testdaten aus einem json file eingelesen und in der Funktion durchgearbeitet.
   zB.:
   ```
   [
       {
           'amount': 10,
           'age' 25,
           'payment_method': 'direct_debit',
           'payment_interval': 12,
           'excepted_points': 196
       },
       { ... }
   ```
      Die Testdaten sollten so umfangreich wie möglich sein und möglichst alle cases abdecken.  

